import json
import os
from os import path
from flask import Flask, send_from_directory, current_app
from flask import Response
from flask import request
from gplaycli import gplaycli
from gpapi.googleplay import RequestError
import config
import requests
from bs4 import BeautifulSoup

application = Flask(__name__)

gpc = gplaycli.GPlaycli(config_file="gplaycli.conf")
gpc.download_folder = os.path.abspath('./'+config.UPLOAD_DIR)


def isset(variable):
    return variable in locals() or variable in globals()


def custom_index(array, compare_function):
    for i, v in enumerate(array):
        if v == compare_function:
            return i
    return -1


@application.route('/' + config.UPLOAD_DIR + '/<appname>', methods=['GET'])
def download(appname):
    uploads = os.path.join(current_app.root_path, config.UPLOAD_DIR)
    return send_from_directory(directory=uploads, filename=appname + ".apk")


@application.route("/favicon.ico", methods=['GET'])
def favicon():
    return ""

retry = 20

@application.route("/detail/<appname>", methods=['GET'])
def getDetail(appname):
    try:
        appinfo = gpc.details([appname])
        if(appinfo == None or appinfo == []):
            return Response("application not found")
        appinfo = appinfo[0]
        resp = Response(json.dumps(appinfo))
        resp.headers["Content-Type"] = "application/json"
        return resp
    except RequestError as e:
        resp = Response(str(e))
        return resp
@application.route("/<appname>", methods=['GET'])
def get(appname):
    try:
        appinfo = gpc.details([appname])
        if(appinfo == None or appinfo == []):
            return Response("application not found")
        appinfo = appinfo[0]
        download_resualt = None
        loop = 0
        while not path.exists("./" + config.UPLOAD_DIR + "/" + appname + ".apk"):
            loop+=1
            download_resualt,det = gpc.download(pkg_todownload=[appname], appinfo=[appinfo])
            if path.exists("./" + config.UPLOAD_DIR + "/" + appname + ".apk"):
                break
            else:
                error = str(download_resualt[0][1])
                if (error.find('DF-DFERH-01') == -1):
                    appinfo['file'] = error
                    print(error)
                    break

            if(loop >= retry):
                appinfo['file'] = "error"
                break

        try:
            if(appinfo['file'] == None or  appinfo['file'] == ""):
                appinfo['file'] = str(request.url_root) + config.UPLOAD_DIR + "/" + appname
        except KeyError:
            appinfo['file'] = str(request.url_root) + config.UPLOAD_DIR + "/" + appname
        URL = 'https://play.google.com/store/apps/details?id='+appname
        page = requests.get(URL)
        soup = BeautifulSoup(page.content, 'html.parser')
        category = soup.find('a',class_='hrTbp R8zArc',itemprop="genre")
        appinfo['category'] = category.text
        cathref = category['href'].split("/")
        appinfo['type'] = (cathref[len(cathref)-1].split("_"))[0]
        if(appinfo['type'] != "GAME"):
            appinfo['type'] = "APPLICATION"
        resp = Response(json.dumps(appinfo))
        resp.headers["Content-Type"] = "application/json"
        return resp
    except RequestError as e:
        resp = Response(str(e))
        return resp
